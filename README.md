This git repository contains solution for both tasks given.
Use "gradle build -Pprofile=dev" or "gradle build -Pprofile=prod" for different results in db.properties file.
For publishing use "gradle publishToMavenLocal".