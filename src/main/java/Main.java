import org.apache.commons.lang3.CharSetUtils;

public class Main {

    public static void main(String[] args) {
        // Prints "Hello, World" to the terminal window.
        System.out.println("Hello, World");
        System.out.println(CharSetUtils.count("aaa", "a-c"));
    }

}